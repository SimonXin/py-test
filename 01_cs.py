import queue
import time
from multiprocessing import Process, Manager

def gen_data(q):
    for i in range(10):
        list = [i*10+ j for j in range(10)]
        print('>>>>>', list)
        q.put(list)
        # time.sleep(5)
    pass

def handle_data(q, batch_size):
    while True:
        data = q.get()
        print('---', data)

        batch_size -= 1
        if batch_size == 0:
            break
        time.sleep(1)

    print('----finish')


if __name__ == '__main__':
    batch_size = 10
    data_queue = Manager().Queue()
    produce = Process(target=gen_data, args=(data_queue,))
    consumer = Process(target=handle_data, args=(data_queue, batch_size))
    produce.start()
    consumer.start()

    produce.join()
    consumer.join()
    pass