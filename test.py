import numpy as np

data1 = ['A-11', 'A-12', 'A-13', 'A-14', 'A-15', 'A-16']
data2 = ['A-21', 'A-22', 'A-23', 'A-24', 'A-25', 'A-26']
data3 = ['A-31', 'A-32', 'A-33', 'A-34', 'A-35', 'A-36']
data4 = ['A-41', 'A-42', 'A-43', 'A-44', 'A-45', 'A-46']
data5 = ['A-51', 'A-52', 'A-53', 'A-54', 'A-55', 'A-56']
data6 = ['A-61', 'A-62', 'A-63', 'A-64', 'A-65', 'A-66']

datac = ['A-71', 'A-72']


def exchange(data1):
    data1 = [data1]
    print(data1)
    # data1 = np.reshape(data1, [2, 3])
    data1 = np.resize(data1, [3, 2])
    print(data1)
    print(data1.shape)
    data1 = data1.T
    # data1 = np.reshape(data1, -1)
    print(data1)
    print(data1.shape)

exchange(data1)

a = np.reshape(data1, [ -1, 2]).tolist()
print('a====\n', a)
b = np.reshape(data2, [ -1, 2]).tolist()
c = [['0']]*3
print('c---', c)
z = np.hstack((a, b))
z = np.hstack((z, c))

data_sum = np.array(z)
print('z....\n', data_sum)

data_sum = data_sum.swapaxes(1, 2)
print('==========\n', data_sum)

data_sum = data_sum.swapaxes(0, 1)
#data_sum = np.transpose(data_sum)
print('----------\n', data_sum)

data_sum = data_sum.swapaxes(2, 1)
print('+++++++++\n', data_sum)

data_sum = data_sum.reshape(-1)
print(data_sum)

print(data_sum.shape)